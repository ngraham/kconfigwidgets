/*
    Copyright (c) 2019 Milian Wolff <mail@milianw.de>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include <QObject>
#include <QTest>
#include <QAbstractItemModel>

#include "kcolorscheme.h"
#include "kcolorschememanager.h"

class KColorSchemeTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void benchConstruction_data()
    {
        KColorSchemeManager manager;
        if (manager.model()->rowCount() <= 1) {
            QSKIP("no scheme files found, cannot run benchmark");
        }

        const auto anyScheme = manager.model()->index(1, 0).data(Qt::UserRole).toString();
        QVERIFY(QFile::exists(anyScheme));

        QTest::addColumn<QString>("file");

        QTest::newRow("default") << QString();
        QTest::newRow("explicit") << anyScheme;
    }

    void benchConstruction()
    {
        QFETCH(QString, file);
        qApp->setProperty("KDE_COLOR_SCHEME_PATH", file);

        QBENCHMARK {
            KColorScheme scheme(QPalette::Active);
        }
    }
};

QTEST_MAIN(KColorSchemeTest)

#include "kcolorschemetest.moc"
